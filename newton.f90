module newton

	! Module Parameters
	implicit none
	integer, parameter :: maxiter = 20
	real(kind=8), parameter :: tol = 1.d-14

contains

	subroutine solve(f, fp, x0, x, iters, debug)
	
		! Estimate the zero of f(x) using Newton's method
		! f is the required function
		! fp is the derivative of the function
		! x0 is the initial guess
		! debug - logical parameter that prints statements to debug the program
		! Output returns the root of the function f and the number of iterations it took
		
		implicit none
		real(kind=8), intent(in) :: x0
		real(kind=8), external :: f, fp
		logical, intent(in) :: debug
		real(kind=8), intent(out) :: x
		integer, intent(out) :: iters
		
		! Declaring local variables
		real(kind=8) ::deltax, fx, fxprime
		integer :: k
		
		! Initial guess
		x = x0
		
		if (debug) then
			print 11, x
11			format('Initial guess, x = ', e20.15)
		end if
		
		! Newton's iteration to find zero of f
		do k = 1,maxiter
			fx = f(x)
			fxprime = fp(x)
			
			if (abs(fx)<tol) then
				exit ! Jump out of the do loop
				endif
			
			!Compute Newton's increment
			deltax = fx/fxprime
			
			!update x
			x = x-deltax
			
			if (debug) then
				print 12, k, x
12				format("After ",i3," iterations, x = ", e20.15)
				endif
		enddo
		
		if (k > maxiter) then
			!might not have converged
			fx = f(x)
			if (abs(fx) > tol) then
				print *, " *** Warning, function has not converged"
				endif
			endif
		
		! Number of iterations taken
		iters = k-1
		
	end subroutine solve
end module newton