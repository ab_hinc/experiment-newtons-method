# README #

This project was studied with the help of [notes](http://faculty.washington.edu/rjl/uwhpsc-coursera/fortran_newton.html) from University of Washington's course

### What is this repository for? ###

* This repository contains the fortran codes that I think will be helpful in learning about [Newton's Method](https://en.wikipedia.org/wiki/Newton's_method) of Estimation of value of a function
* Final version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Needs Gfortran.
* Configuration : Edit the functions.f90 file to change the function whose roots are required. Change the maxiter value within the module in newton.90 file to change the number of iterations to be done.
* Dependencies : None
* Database configuration : Not required
* How to run tests : Run the make file and the output will be saved in output.txt file

### Who do I talk to? ###

* Karthik Mohan ( karthik.mohan390@gmail.com )